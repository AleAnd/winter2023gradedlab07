import java.util.Scanner;

public class TicTacToeGame{
	public static void main(String[] args){
		System.out.println("Welcome to my tic tac toe game!");
		
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		Scanner keyboard = new Scanner(System.in);
		
		while(gameOver != true){
			System.out.println(board);
			
			if(player == 1){
				playerToken = Square.X;
			} else {
				playerToken = Square.O;
			}
			
			
			System.out.println("Input row:");
			int userRow = keyboard.nextInt()-1;
			System.out.println("Input column:");
			int userCol = keyboard.nextInt()-1;
			
			//validation
			while(board.placeToken(userRow, userCol, playerToken) == false){
				System.out.println("Invalid input.");
				System.out.println("Input row:");
				userRow = keyboard.nextInt()-1;
				System.out.println("Input column:");
				userCol = keyboard.nextInt()-1;
			}
			
			if(board.checkIfFull() == true){
				System.out.println(board);
				System.out.println("It's a tie!");
				gameOver = true;
			} else if(board.checkIfWinning(playerToken)){
				System.out.println(board);
				System.out.println("Player " + player + " has won!");
				gameOver = true;
			} else {
				player++;
				if(player > 2){
					player = 1;
				}
			}
			
		}
	}
}