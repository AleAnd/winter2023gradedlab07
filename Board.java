public class Board{
	private Square[][] tictactoeBoard;
	
	public Board(){
		tictactoeBoard = new Square[3][3];
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard.length; j++){
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString(){
		String boardString = "";
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard.length; j++){
				boardString += tictactoeBoard[i][j] + " ";
				if(j == 2){
					boardString += "\n";
				}
			}
		}
		return boardString;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if(row > 2 || row < 0){
			return false;
		}
		if(col > 2 || col < 0){
			return false;
		}
		if(tictactoeBoard[row][col] == Square.BLANK){
			tictactoeBoard[row][col] = playerToken;
			return true;
		} else {
			return false;
		}
	}
	public boolean checkIfFull(){
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard.length; j++){
				if(tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		} 
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i = 0; i < 3; i++){
			if(tictactoeBoard[i][0] == playerToken
			&& tictactoeBoard[i][1] == playerToken
			&& tictactoeBoard[i][2] == playerToken){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		for(int i = 0; i < 3; i++){
			if(tictactoeBoard[0][i] == playerToken
			&& tictactoeBoard[1][i] == playerToken
			&& tictactoeBoard[2][i] == playerToken){
				return true;
			}
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken){
		if(checkIfWinningHorizontal(playerToken) == true || checkIfWinningVertical(playerToken) == true){
			return true;
		}else{
			return false;
		}
	}
}

